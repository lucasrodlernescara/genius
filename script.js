var sequence    = [];
var response    = [];
var round       = 0;
var colors      = ['red', 'green', 'yellow', 'blue'];
var position    = 0;
var showSequence;

function generateSequence() {
    var x = Math.floor((Math.random() * colors.length));
    var t = 1000 - (round * 10);
    if (t <= 250) {
        t = 250;
    }
    sequence.push(colors[x]);
    position = 0;
    showSequence = setInterval(function(){
        if (position > 0) {
            document.getElementById(sequence[(position-1)]).classList.remove('active');
        }
        if (position < sequence.length) {
            setTimeout(function() {
                document.getElementById('audio').src = 'sounds/' +sequence[position]+ '.mp3';
                document.getElementById('audio').play();
                document.getElementById(sequence[position]).classList.add('active');
                position++;
            },250);
        }else{
            clearInterval(showSequence);
        }
    }, t);

}

function validateColor(color) {
    response.push(color);
    document.getElementById('audio').src = 'sounds/' +color+ '.mp3';
    document.getElementById('audio').play();
    if (response[response.length - 1] == sequence[response.length - 1]) {
        if (response.length == sequence.length) {
            round++;
            document.getElementById('round').innerHTML = round;
            document.getElementById('round').classList.add('animated');
            document.getElementById('round').classList.add('bounceIn');
            setTimeout(function() {
                document.getElementById('round').classList.remove('animated');
                document.getElementById('round').classList.remove('bounceIn');
                response = [];
                generateSequence();
            },500);
        }
        return true;
    }else{
        restartGame();
    }
}

function startGame() {
    document.getElementById('content').classList.remove('animated');
    document.getElementById('content').classList.remove('shake');
    document.getElementById('content').classList.remove('disabled');
    document.getElementById('button').classList.add('disabled');
    document.getElementById('button').setAttribute('disabled', true);
    document.getElementById('restart').classList.remove('disabled');
    document.getElementById('restart').removeAttribute('disabled');
    for (var i = 0; i < colors.length; i++) {
        var id = colors[i];
        document.getElementById(id).onclick = setColor(id);
    }
    generateSequence(0);
}

function setColor(id) {
    return function() {
        validateColor(id);
    }
}

function restartGame() {
    clearInterval(showSequence);
    document.getElementById('audio').src = 'sounds/lose.mp3';
    document.getElementById('audio').play();
    response    = [];
    sequence    = [];
    round       = 0;
    for (var i = 0; i < colors.length; i++) {
        document.getElementById(colors[i]).onclick = function() {};
    }
    document.getElementById('round').innerHTML = round;
    document.getElementById('content').classList.add('animated');
    document.getElementById('content').classList.add('shake');
    document.getElementById('content').classList.add('disabled');
    document.getElementById('restart').classList.add('disabled');
    document.getElementById('restart').setAttribute('disabled', true);
    document.getElementById('button').classList.remove('disabled');
    document.getElementById('button').removeAttribute('disabled');
}
